# Machine learning project on Air quality

I have to use regression method on a database

## What we have to do

Regularized regression methods are, in a statistician’s toolbox, one of the most useful tools. In
this project you will be called to:



Detail the model selection methodology you will apply, which can include (among others):

• using a baseline model;

• performing variable selection through stepwise and/or penalized regression methods;

• using one method that is the extension of what was seen in the course. Select among
the KNN, K-means + Regression, group-Lasso, or Elastic-Net methods.

## Authors

Lucie BECHU and Adrien BOLLING
